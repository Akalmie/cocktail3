package com.akalmie.cocktail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView

import com.akalmie.cocktail.data.CocktailService
import com.akalmie.cocktail.data.Drinks
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.Exception


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.d("MainActivityDebug", "coucou je suis dans le onCreate()")
        val textView = findViewById<TextView>(R.id.textView2)
        val textView3 = findViewById<TextView>(R.id.textView3)

        val drinkingPicture = findViewById<ImageView>(R.id.imageView)
       // val button = findViewById<Button>(R.id.Like)
        try {
            val retrofit = Retrofit.Builder()
                .baseUrl("https://www.thecocktaildb.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            val cocktailService: CocktailService = retrofit.create(CocktailService::class.java)
            val call = cocktailService.getCocktailByName(search = "random")
            call.enqueue(object : Callback<Drinks> {
                override fun onResponse(call: Call<Drinks>, response: Response<Drinks>) = with(response.body()?.drinks?.first()){
                    textView.text = this?.strDrink
                    textView3.text = this?.strIngredient1 + " + " + this?.strIngredient2 + " + " + this?.strIngredient3 + " + " + this?.strIngredient4  + " + " + this?.strIngredient5+ " + " + this?.strIngredient6 + " + " + this?.strIngredient7
                    Picasso.get().load(response.body()?.drinks?.first()?.strDrinkThumb).into(drinkingPicture)
                }
                override fun onFailure(call: Call<Drinks>, throwable: Throwable) {
                    Log.e("MainActivityError", throwable.message.toString())
                }
            })
        } catch (e: Exception) {
            Log.e("MainActivityError", e.message.toString())
        }
    }

    override fun onStart() {
        super.onStart()
        Log.d("MainActivityDebug", "coucou je suis dans le onStart()")
    }

    override fun onPause() {
        super.onPause()
        Log.d("MainActivityDebug", "coucou je suis dans le onPause()")
    }

    override fun onStop() {
        super.onStop()
        Log.d("MainActivityDebug", "coucou je suis dans le onStop()")
    }

    override fun onRestart() {
        super.onRestart()
        Log.d("MainActivityDebug", "coucou je suis dans le onRestart()")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("MainActivityDebug", "coucou je suis dans le onDestroy()")
    }
}